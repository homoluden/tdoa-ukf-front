/* global signalR */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const POINT_BORDER_COLOR = `#249EBF`;
const POINT_BG_COLOR = `white`;
const DATASET_DEFAULT = {
  enabled: false,
  pointBorderColor: POINT_BORDER_COLOR,
  pointBackgroundColor: POINT_BG_COLOR,
  borderWidth: 1,
  data: []
};

export default {
  namespaced: true,
  state: {
    connection: null,
    labels: [0.0, 0.02, 0.04],
    datasets: [
      {
        ...DATASET_DEFAULT,
        label: `Δ TDoA 1-2 (measured vs calculated)`,
        borderColor: `#222B45`,
        enabled: true,
        data: []
      },
      {
        ...DATASET_DEFAULT,
        label: `TDoA 1-2 (measured)`,
        borderColor: `#3366FF`,
        enabled: true,
        data: []
      },
      {
        ...DATASET_DEFAULT,
        label: `TDoA 1-2 (calculated)`,
        borderColor: `#00D68F`,
        enabled: true,
        data: []
      },
      // { ...DATASET_DEFAULT, label: `TDoA 1-3`, borderColor: `#3366FF` },
      // { ...DATASET_DEFAULT, label: `TDoA 1-4`, borderColor: `#00D68F` },
      { ...DATASET_DEFAULT, label: `TDoA 1-5`, borderColor: `#FF3366` },
      { ...DATASET_DEFAULT, label: `TDoA 1-6`, borderColor: `#FFC94D` },
      { ...DATASET_DEFAULT, label: `TDoA 1-7`, borderColor: `#326ADA` },
      { ...DATASET_DEFAULT, label: `TDoA 1-8`, borderColor: `#433E90` },
      { ...DATASET_DEFAULT, label: `TDoA 1-9`, borderColor: `#7B0099` },
      { ...DATASET_DEFAULT, label: `TDoA 2-3`, borderColor: `#222B45` },
      { ...DATASET_DEFAULT, label: `TDoA 2-4`, borderColor: `#F77FBE` },
      { ...DATASET_DEFAULT, label: `TDoA 2-5`, borderColor: `#FE28A2` },
      { ...DATASET_DEFAULT, label: `TDoA 2-6`, borderColor: `#0057B8` },
      { ...DATASET_DEFAULT, label: `TDoA 2-7`, borderColor: `#00B5E2` },
      { ...DATASET_DEFAULT, label: `TDoA 2-8`, borderColor: `#64A70B` },
      { ...DATASET_DEFAULT, label: `TDoA 2-9`, borderColor: `#C4D600` },
      { ...DATASET_DEFAULT, label: `TDoA 3-4`, borderColor: `#FFE900` },
      { ...DATASET_DEFAULT, label: `TDoA 3-5`, borderColor: `#302155` },
      { ...DATASET_DEFAULT, label: `TDoA 3-6`, borderColor: `#2628A4` },
      { ...DATASET_DEFAULT, label: `TDoA 3-7`, borderColor: `#F2350B` },
      { ...DATASET_DEFAULT, label: `TDoA 3-8`, borderColor: `#3F3F3F` },
      { ...DATASET_DEFAULT, label: `TDoA 3-9`, borderColor: `#383F4F` },
      { ...DATASET_DEFAULT, label: `TDoA 4-5`, borderColor: `#2B5B4B` },
      { ...DATASET_DEFAULT, label: `TDoA 4-6`, borderColor: `#AC3B3F` },
      { ...DATASET_DEFAULT, label: `TDoA 4-7`, borderColor: `#880044` },
      { ...DATASET_DEFAULT, label: `TDoA 4-8`, borderColor: `#19535F` },
      { ...DATASET_DEFAULT, label: `TDoA 4-9`, borderColor: `#FFA630` },
      { ...DATASET_DEFAULT, label: `TDoA 5-6`, borderColor: `#0C0C0C` },
      { ...DATASET_DEFAULT, label: `TDoA 5-7`, borderColor: `#190F28` },
      { ...DATASET_DEFAULT, label: `TDoA 5-8`, borderColor: `#470024` },
      { ...DATASET_DEFAULT, label: `TDoA 5-9`, borderColor: `#6B0504` },
      { ...DATASET_DEFAULT, label: `TDoA 6-7`, borderColor: `#7B0D1E` },
      { ...DATASET_DEFAULT, label: `TDoA 6-8`, borderColor: `#395A7C` },
      { ...DATASET_DEFAULT, label: `TDoA 6-9`, borderColor: `#212F55` },
      { ...DATASET_DEFAULT, label: `TDoA 7-8`, borderColor: `#000000` },
      { ...DATASET_DEFAULT, label: `TDoA 7-9`, borderColor: `#2B3300` },
      { ...DATASET_DEFAULT, label: `TDoA 8-9`, borderColor: `#332C44` }
    ]
  },
  mutations: {
    setConnection(state, connection) {
      state.connection = connection;
    },
    addSimData(state, simData) {
      const lbl = simData.label;
      const tdoaDiff = simData.tdoaDiff;
      state.labels.push(lbl);

      const measured = tdoaDiff.measured.td["24"];
      // const calculated = 2500.0 + 1.9 * (tdoaDiff.calculated.td["18"] + 680.0);
      const calculated = 0.0 + 1.0 * (tdoaDiff.calculated.td["24"] + 0.0);
      const diff = measured - calculated;
      state.datasets[0].data.push(diff);
      state.datasets[1].data.push(measured);
      state.datasets[2].data.push(calculated);
      // state.datasets[2].data.push(diff.calculated.td["15"]);
    }
  },
  actions: {
    runSim({ state }) {
      var { connection } = state;

      connection &&
        connection.invoke("RunTdoaSim").catch(err => {
          return console.error(err.toString());
        });
    },

    initSocket({ state, commit }) {
      if (state.connection) {
        return state.connection;
      }

      return new Promise((resolve, reject) => {
        let tryNum = 0;

        const checkSignalR = async () => {
          tryNum++;

          if (tryNum > 20) {
            const err = new Error(`SignalR connection failed!`);
            console.error(err);
            reject(err);
            return;
          }

          if (!window.signalR) {
            console.info(`Connecting to signalR...`);
            setTimeout(async () => {
              await checkSignalR();
            }, 1000);
            return;
          }

          // TODO: move URL to configuration
          const connection = new signalR.HubConnectionBuilder()
            .withUrl("http://localhost:5000/dataHub")
            .configureLogging(signalR.LogLevel.Information)
            .build();

          // Responses from backend
          connection.on("addSimData", data => {
            commit(`addSimData`, data);
            console.info(`[SIM RX] `, data);
          });

          try {
            await connection.start();
            console.assert(
              connection.state === signalR.HubConnectionState.Connected
            );
            console.log("connected");
          } catch (err) {
            console.assert(
              connection.state === signalR.HubConnectionState.Disconnected
            );
            console.log(err);
            setTimeout(() => checkSignalR(), 1000);
          }

          commit(`setConnection`, connection);
          resolve(connection);
        };

        checkSignalR();
      });
    },

    shutdownSocket({ state, commit }) {
      if (!state.connection) {
        return;
      }

      console.info(`Shutting down signalR...`);
      state.connection.stop();
      commit(`setConnection`, null);
    }
  }
};
