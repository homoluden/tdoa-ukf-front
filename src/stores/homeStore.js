/* global signalR */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    connection: null,
    message: ``,
    pongs: []
  },
  mutations: {
    updateMessage(state, value) {
      state.message = value;
    },
    setConnection(state, connection) {
      state.connection = connection;
    },
    addPong(state, text) {
      state.pongs.push({
        key: state.pongs.length,
        text: text
      });
    }
  },
  actions: {
    sendMessage({ state }) {
      var { message, connection } = state;

      connection &&
        connection.invoke("Ping", message).catch(err => {
          return console.error(err.toString());
        });
    },

    initSocket({ state, commit }) {
      if (state.connection) {
        return state.connection;
      }

      return new Promise((resolve, reject) => {
        let tryNum = 0;

        const checkSignalR = async () => {
          tryNum++;

          if (tryNum > 20) {
            const err = new Error(`SignalR connection failed!`);
            console.error(err);
            reject(err);
            return;
          }

          if (!window.signalR) {
            console.info(`Connecting to signalR...`);
            setTimeout(async () => {
              await checkSignalR();
            }, 1000);
            return;
          }

          // TODO: move URL to configuration
          const connection = new signalR.HubConnectionBuilder()
            .withUrl("http://localhost:5000/dataHub")
            .configureLogging(signalR.LogLevel.Information)
            .build();

          // Responses from backend
          connection.on("addPong", data => {
            commit(`addPong`, data);
            console.info(`[TDoA RX] `, data);
          });

          try {
            await connection.start();
            console.assert(
              connection.state === signalR.HubConnectionState.Connected
            );
            console.log("connected");
          } catch (err) {
            console.assert(
              connection.state === signalR.HubConnectionState.Disconnected
            );
            console.log(err);
            setTimeout(() => checkSignalR(), 1000);
          }

          commit(`setConnection`, connection);
          resolve(connection);
        };

        checkSignalR();
      });
    },

    shutdownSocket({ state, commit }) {
      if (!state.connection) {
        return;
      }

      console.info(`Shutting down signalR...`);
      state.connection.stop();
      commit(`setConnection`, null);
    }
  }
};
