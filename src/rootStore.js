import Vue from "vue";
import Vuex from "vuex";

import tdoaSim from "./stores/tdoaSimStore";
import home from "./stores/homeStore";

Vue.use(Vuex);

const rootStore = new Vuex.Store({
  modules: {
    tdoaSim: tdoaSim,
    home: home
  }
});

export default rootStore;
