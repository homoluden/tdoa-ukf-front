import Vue from "vue";
import Meta from "vue-meta";
import App from "./App.vue";
import router from "./router";
import store from "./rootStore";
import "./registerServiceWorker";

Vue.use(Meta);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
